//LRS.S.MODULO.ESP8266

#define BLYNK_PRINT Serial
#include <BlynkSimpleEsp8266.h>
#include <ESP8266WiFi.h>

#include <DHT.h>

char auth[] = "vKq7X_JULtQWYl_Yw79pmWdd-Pr5z4TS";
char ssid[] = "INFINITUM7366_2.4";
char pass[] = "yiMSJkkEm3";
 
#define DHTPIN 2          //GPI0 02 O D4
 #define DHTTYPE DHT11     // DHT 11
DHT dht(DHTPIN, DHTTYPE);
BlynkTimer timer;
 
void sendSensor()
{
  float h = dht.readHumidity();
  float t = dht.readTemperature();
   if (isnan(h) || isnan(t)) {
    Serial.println("Failed to read from DHT sensor!");
    return;
  }
Blynk.virtualWrite(V5, t);
Blynk.virtualWrite(V6, h);
}
 
void setup()
{
  Serial.begin(9600);
  Blynk.begin(auth, ssid, pass);
  dht.begin();
  timer.setInterval(1000L, sendSensor);
}
 
void loop()
{
  Blynk.run();
  timer.run();
}
